const glob = require('glob')
const webpack = require('webpack')
const path = require('path')
const MiniExtractTextPlugin = require("mini-css-extract-plugin")
const UglifyJsPlugin = require("uglifyjs-webpack-plugin")
const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin")
const CopyWebpackPlugin = require("copy-webpack-plugin")
const VueLoaderPlugin = require('vue-loader/lib/plugin')

let NODE_ENV = 'production'
if (process.env.NODE_ENV) {
    NODE_ENV = process.env.NODE_ENV.replace(/^\s+|\s+$/g, "")
}

let serverFiles = glob.sync('./server/**/*.js')
let clientFiles = glob.sync('./client/src/**/*.js')

const serverConfig = {
    target: 'node',
    mode: NODE_ENV,
    entry: {
        'server/index': ['babel-polyfill', ...serverFiles] 
    },
    output: {
        path: path.resolve(__dirname, 'build'),
        filename: '[name].js',
        libraryTarget: 'commonjs'
    },
    externals: [ 
        /^(?!\.|\/).+/i, 
    ],
    watch: NODE_ENV == 'development',
    module: {
        rules: [{
                test: /\.js$/,
                exclude: [/node_modules/],
                loader: 'babel-loader',
                query: {
                    presets: ['es2015', 'stage-0']
                }
            }
        ]
    },
    optimization: {
        minimize: true,
        minimizer: [
          new UglifyJsPlugin({
            cache: true,
            parallel: true,
            sourceMap: false
          }),
          new OptimizeCSSAssetsPlugin({})
        ]
    }
};

const clientConfig = {
    target: 'web',
    mode: NODE_ENV,
    entry: {
        'client/index': clientFiles,
        'client/ui/app': './client/ui/app/index.js'
    },
    output: {
        path: path.resolve(__dirname, 'build'),
        filename: '[name].js'
    },
    watch: NODE_ENV == 'development',
    module: {
        rules: [{
                test: /\.js$/,
                exclude: [/node_modules/],
                loader: 'babel-loader',
                query: {
                    presets: ['es2015', 'stage-0']
                }
            },
            {
                test: /\.vue$/,
                loader: 'vue-loader'
            },
            {
                test: /\.scss$/,
                use: [
                    MiniExtractTextPlugin.loader,
                    'css-loader',
                    'sass-loader'
                ]
            }
        ]
    },
    resolve: {
        alias: {
            'vue$': 'vue/dist/vue.esm.js'
        },
        extensions: ['.js', '.vue', '.json']
    },
    optimization: {
        minimize: true,
        minimizer: [
          new UglifyJsPlugin({
            cache: true,
            parallel: true,
            sourceMap: false
          }),
          new OptimizeCSSAssetsPlugin({})
        ]
    },
    plugins: [
        new webpack.EnvironmentPlugin('NODE_ENV'),
        new MiniExtractTextPlugin({
            filename: "./[name].css",
            allChunks: true
        }),
        new CopyWebpackPlugin([{
            from: './client/ui/fonts',
            to: './client/ui/fonts'
          },
          {
            from: './client/ui/images',
            to: './client/ui/images'
          },
          {
            from: './client/ui/index.html',
            to: './client/ui/index.html'
          },
          {
            from: './client/ui/app/EventManager.js',
            to: './client/ui'
          }
        ]),
        new VueLoaderPlugin()
    ]
};

module.exports = [serverConfig, clientConfig];
