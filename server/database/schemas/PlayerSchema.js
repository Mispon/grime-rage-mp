import mongoose from '../Mongo'

const Schema = mongoose.Schema

// Данные аккаунта
const Account = new Schema({
    login: String,
    password: String,
    salt: String,
    email: String, 
    creation: Date,       
    lastLogin: Date,
    totalInGame: Number,
    daysInRow: Number,
    premium: Date,
    referal: String
})

// Данные работы
const Job = new Schema({
    type: Number,
    level: {type: Number, default: 1},
    exp: {type: Number, default: 0},
    salary: {type: Number, default: 0},
})

// Данные личного транспорта
const Vehicle = new Schema({})

// Параметры внешности
const Appearance = new Schema({
    father: Number,
    mother: Number,
    similarity: Number,
    skinSimilarity: Number,
    hair: Number,
    hairColor: Number,
    highlightColor: Number,
    beard: Number,
    beardColor: Number,
    eyeColor: Number,
    eyebrows: Number,
    makeup: Number,
    blush: Number,
    blushColor: Number,
    lipstick: Number,
    lipstickColor: Number,
    freckles: Number,
    complexion: Number,
    ageing: Number,
    features: [Number],
})

// Прогресс в автошколах
const DriverInfo = new Schema({
    isDutyPaid: {type: Boolean, default: false},
    isTheoryPassed: {type: Boolean, default: false},
    licenses: [Number],
    timeToNextTry: Date
})

// Модель одежды
const Clothes = new Schema({
    id: Number,
    name: String,
    slot: Number,
    drawable: Number,
    texture: Number,
    textures: [Number],
    torso: Number,
    undershirt: Number,
    dressed: Boolean,
    isProp: Boolean
})

/*
** Схема данных игрока
*/
const PlayerSchema = new Schema({
    id: Number,
    name: {type: String, default: ''},
    gender: Number,
    money: {type: Number, default: 0},
    bank: {type: Number, default: 0},
    level: {type: Number, default: 1},
    exp: {type: Number, default: 0},
    health: {type: Number, default: 100.0},
    satiety: {type: Number, default: 100.0},
    thirst: {type: Number, default: 100.0},
    position: new Schema({x: Number, y: Number, z: Number}),
    dimension: {type: Number, default: 0},
    account: Account,
    jobs: [Job],
    vehicles: [Vehicle],
    clothes: [Clothes],
    appearance: Appearance,
    driver: DriverInfo
})

const PlayerModel = mongoose.model('Players', PlayerSchema)

export default PlayerModel