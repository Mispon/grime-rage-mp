import data from './driving-school-data.json'
import * as Events from '../../../../global/constants/ServerEvents'
import { NPC_MENU, DRIVING_EXAM } from '../../../../global/constants/PlayerVariables'
import { GENERAL_VEHICLE_KEY } from '../../../../global/constants/VehicleVariables'
import * as MainPosition from '../../../constants/MainPositions'
import VehicleTypes from '../../../enums/VehicleType'
import LicenseType from '../../../../global/enums/LicenseType'
import PointCreator from '../../../utils/PointCreator'
import Notify from '../../../utils/Notify'
import DateHelper from '../../../utils/DateHelper'
import VehicleManager from '../../vehicles/VehiclesManager'
import moment from 'moment'
import { locale } from '../../../../global/Locale'

const PLACE_NAME = locale('place', 'drivingSchool')
const NPC_NAME = locale('npc', 'drivingSchool')
const DUTY_AMOUNT = 300
const ON_DUTY_ACCEPT = 'AcceptDuty'

const BRIBE_AMOUNT = 1000
const BRIBE_LIC_TYPE = 'BribeLicenseKey'
const ON_BRIBE_ACCEPT = 'AcceptBribe'

// Инициализирует автошколу
function initialize() {
    mp.events.add({
        [Events.START_THEORY_EXAM]: startTheory,
        [Events.THEORY_EXAM_FINISH]: finishTheory,
        [Events.START_PRACTICE_EXAM]: startPractice,
        [Events.BUY_LICENSE]: buyLicense,
        [ON_DUTY_ACCEPT]: onDutyAccept,
        [ON_BRIBE_ACCEPT]: onBribeAccept,
    })
    createVehicles()
    createNpc()
}

// Запускает теоретический экзамен
function startTheory(player) {
    // todo: проверить время сл. попытки
    if (!(isDutyPaid(player) && timeAvailable(player))) return
    if (player.props.driver.isTheoryPassed) {
        Notify.showError(player, getMessage('theoryAlreadyPassed'))
        return
    }
    player.call(Events.START_THEORY_EXAM)
}

// Обработчик завершения теории
function finishTheory(player, result) {
    if (!result) {
        let nextTryTime = moment().add(1, 'hours')
        player.props.driver.timeToNextTry = nextTryTime
        Notify.showError(player, `${getMessage('fail')} ${DateHelper.format(nextTryTime)}`, 10000)
        return
    }
    player.props.driver.isTheoryPassed = true
    Notify.showSuccess(player, getMessage('theorySuccess'))
}

// Запускает практический экзамен
function startPractice(player, type) {
    if (hasLicense(player, type) || !(isDutyPaid(player) && timeAvailable(player))) return
    if (!player.props.driver.isTheoryPassed) {
        Notify.showError(player, getMessage('theoryNotPassed'))
        return
    }
    player.set(DRIVING_EXAM, type)
    Notify.showInfo(player, getMessage('toStartPractice'))
}

// Обработчик покупки лицензии
function buyLicense(player, type) {
    if (hasLicense(player, type) || !player.enoughMoney(BRIBE_AMOUNT)) return
    player.set(BRIBE_LIC_TYPE, type)
    const dialogDesc = `${getMessage('bribeDialogDesc')} ${BRIBE_AMOUNT}$`
    Notify.showDialog(player, getMessage('bribeDialogHeader'), dialogDesc, ON_BRIBE_ACCEPT)
}

// Обработчик оплаты пошлины
function onDutyAccept(player) {
    if (!player.enoughMoney(DUTY_AMOUNT)) return
    player.setMoney(-DUTY_AMOUNT)
    player.props.driver.isDutyPaid = true
    Notify.showInfo(player, getMessage('dutyPaid'))
}

// Обработчик подтверждения покупки лицензии
function onBribeAccept(player) {
    player.setMoney(-BRIBE_AMOUNT)
    player.props.driver.licenses.push(player.get(BRIBE_LIC_TYPE))
    Notify.showSuccess(player, locale('player', 'newLicense'))
}

// Проверяет, что время попытка сдачи корректно
function timeAvailable(player) {
    const nextTryTime = player.props.driver.timeToNextTry
    if (nextTryTime && !DateHelper.timeLeft(nextTryTime)) {
        Notify.showError(player, `${getMessage('timeNotYet')} ${DateHelper.format(nextTryTime)}`)
        return false
    }
    return true
}

// Проверяет, оплачена ли пошлина
function isDutyPaid(player) {
    if (player.props.driver.isDutyPaid) return true
    const dialogHeader = getMessage('dutyDialogHeader')
    const dialogDesc = `${getMessage('dutyDialogDesc')} ${DUTY_AMOUNT}$`
    Notify.showDialog(player, dialogHeader, dialogDesc, ON_DUTY_ACCEPT)
    return false
}

// Проверяет, имеется ли данный тип лицензии у игрока
function hasLicense(player, type) {
    if (player.hasLicense(type)) {
        Notify.showError(player, locale('player', 'hasLicense'))
        return true
    }
    return false
}

// Возвращает сообщение автошколы
function getMessage(value) {
    return locale('drivingSchool', value)
}

// Создает учебный транспорт
function createVehicles() {
    data.vehicles.forEach(v => {
        v.numberPlate = 'SCHOOL'        
        let vehicle = VehicleManager.createGeneral(v)
        vehicle.set(GENERAL_VEHICLE_KEY, VehicleTypes.drivingSchool)
        const type = v.model == 'blista' ? LicenseType.lightVehicle : LicenseType.heavyVehicle
        vehicle.set(DRIVING_EXAM, type)
    })
}

// Создает обработчик нпс
function createNpc() {
    const npc = data.npc
    PointCreator.createBlip(MainPosition.DrivingSchool, 545, 3, PLACE_NAME)
    let shape = PointCreator.createNpc(npc.model, MainPosition.DrivingSchool, npc.heading, NPC_NAME)
    shape.onEnter = comeToNpc
    shape.onExit = moveAwayFromNpc
}

// Игрок подошел к нпс
function comeToNpc(player) {
    if (player.vehicle) return
    Notify.showHint(player, locale('hint', 'npcTalk'), NPC_NAME)
    player.set(NPC_MENU, true)
    player.call(Events.SETUP_MENU, [JSON.stringify({name: 'DrivingSchool', data: {}})])    
}

// Игрок отошел от нпс
function moveAwayFromNpc(player) {
    if (player.vehicle) return
    Notify.hideHint(player)
    player.reset(NPC_MENU)
}

initialize()