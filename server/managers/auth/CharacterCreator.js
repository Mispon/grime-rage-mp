import * as Events from '../../../global/constants/ServerEvents'
import PlayerProvider from '../../database/providers/PlayerProvider'

/*
** Серверная логика создания персонажа
*/
const position = new mp.Vector3(402.8664, -996.4108, -99.0)
const heading = -185.0

const startingPosition = new mp.Vector3(113.25, -1944.19, 20.73)

mp.events.add({
    [Events.CHAR_CREATE]: createCharacter
})

// Переносит игрока в редактор персонажа
export const startCreation = player => {
    player.position = position
    player.heading = heading
    player.dimension = player.props.id
    player.call(Events.CHAR_SHOW_CREATOR)
}

// Обработчик завершения редактирования
function createCharacter(player, data) {
    data = JSON.parse(data)    
    const name = `${data.name} ${data.surname}`
    PlayerProvider.isNameUsed(name).then(used => {
        if (used) {
            player.call(Events.CALL_UI, [Events.CHAR_NAME_USE])
            return
        }
        player.name = player.props.name = name
        player.props.gender = data.gender
        player.props.appearance = convertDataToSchema(data)
        applyAppearance(player)
        finishCreation(player)
    })
}

// Завершение создания персонажа
function finishCreation(player) {    
    player.position = startingPosition
    player.dimension = 0
    player.props.position = player.position
    player.giveDefaultClothes()
    player.call(Events.CHAR_HIDE_CREATOR)
    player.call(Events.CALL_UI, [Events.SHOW_HUD])
    player.logged = true
}

// Применяет ностройки внешности
export const applyAppearance = player => {
    const look = player.props.appearance
    player.model = mp.joaat(player.isMale() ? 'mp_m_freemode_01' : 'mp_f_freemode_01')
    player.setCustomization(
        player.isMale(),
        look.mother, look.father, 0,
        look.mother, look.father, 0,
        look.similarity, look.skinSimilarity, 0.0,
        look.eyeColor, look.hairColor, look.highlightColor,
        look.features
    )
    player.setClothes(2, look.hair, 0, 2)
    applyOverlays(player, look)
}

// Применяет настройки головы
function applyOverlays(player, look) {
    setHeadOverlay(player, 1, look.beard, look.beardColor)
    setHeadOverlay(player, 2, look.eyebrows)
    setHeadOverlay(player, 3, look.ageing)
    setHeadOverlay(player, 4, look.makeup)
    setHeadOverlay(player, 5, look.blush, look.blushColor)
    setHeadOverlay(player, 6, look.complexion)
    setHeadOverlay(player, 8, look.lipstick, look.lipstickColor)
    setHeadOverlay(player, 9, look.freckles)
}

// Устанавливает особенности внешности
function setHeadOverlay(player, index, overlayId, color = 0) {
    if (overlayId === -1) overlayId = 255
    player.setHeadOverlay(index, [overlayId, 1.0, color, 0])
}

// Конвертит модель редактора в схему хранения данных
function convertDataToSchema(data) {
    return {
        father: data.genetics.father,
        mother: data.genetics.mother,
        similarity: data.genetics.similarity,
        skinSimilarity: data.genetics.skinSimilarity,
        hair: data.hair.model,
        hairColor: data.hair.color,
        highlightColor: data.hair.highlightColor,
        beard: data.hair.beard,
        beardColor: data.hair.beardColor,
        eyeColor: data.face.eyeColor,
        eyebrows: data.face.eyebrows,
        makeup: data.face.makeup,
        blush: data.face.blush,
        blushColor: data.face.blushColor,
        lipstick: data.face.lipstick,
        lipstickColor: data.face.lipstickColor,
        freckles: data.face.freckles,
        complexion: data.face.complexion,
        ageing: data.face.ageing,
        features: data.features
    }
}