import PlayerProvider from '../database/providers/PlayerProvider'

/*
** Синхранизация кэшей сервера с базой
*/
class SyncManager {
    constructor() {
        if (!SyncManager.instance) {
            SyncManager.instance = this
            this.syncTimer = setInterval(() => {
                try {
                    this.synchronize()
                } catch (e) {console.log(e.message)}
            }, 30000)
        }
        return SyncManager.instance
    }

    // Синхронизация данных
    synchronize() {
        this.syncPlayersData()
        this.syncVehicles()
        this.syncHouses()
    }

    // Синхронизация данных игроков
    syncPlayersData() {
        mp.players.forEach(player => {
            if (!player.logged) return
            PlayerProvider.save(player)
                .catch(reason => {
                    console.log(`Не удалось сохранить данные игрока "${player.props.name}".\r\nПричина: ${reason}`)
                })
        })
    }

    // Синхронизация данных транспорта
    syncVehicles() {

    }

    // Синхронизация данных домов
    syncHouses() {

    }
}

const instance = new SyncManager()

export default instance