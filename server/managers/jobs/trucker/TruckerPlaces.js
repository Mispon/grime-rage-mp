import data from './trucker-data.json'
import * as Events from '../../../../global/constants/ServerEvents'
import * as PlayerKey from '../../../../global/constants/PlayerVariables'
import { GENERAL_VEHICLE_KEY } from '../../../../global/constants/VehicleVariables'
import VehiclesManager from '../../vehicles/VehiclesManager'
import JobType from '../../../enums/JobType'
import VehicleTypes from '../../../enums/VehicleType'
import LicenseType from '../../../../global/enums/LicenseType'
import DeliveryContractType from '../../../../global/enums/DeliveryContractType'
import PointCreator from '../../../utils/PointCreator'
import Notify from '../../../utils/Notify'
import Misc from '../../../utils/Misc'
import { applyWorkClothes } from '../JobClothesController'
import { locale } from '../../../../global/Locale'

/*
** Рабочие места дальнобойщиков
*/

const MIN_LVL = 7
const PLACE_NAME = locale('place', 'trucker')
const NAME_KEY = 'NpcName'
const CONTRACTS_KEY = 'NpcContracts'

const contracts = [
    {type: DeliveryContractType.Corn, reward: 300, target: {x: 2842.14, y: 4702.57, z: 45.94}},
    {type: DeliveryContractType.Crude, reward: 300, target: {x: 2687.37, y: 1367.84, z: 23.52}},
    {type: DeliveryContractType.Tools, reward: 300, target: {x: -593.64, y: 5298.67, z: 69.21}},
    {type: DeliveryContractType.WorkshopWood, reward: 300, target: {x: 1379.25, y: -2065.66, z: 51.49}},
    {type: DeliveryContractType.BuildingsWood, reward: 300, target: {x: -494.59, y: -956.44, z: 22.45}},
    {type: DeliveryContractType.Dyes, reward: 300, target: {x: 749.23, y: -1353.32, z: 25.86}},
    {type: DeliveryContractType.Fuel, reward: 300, target: {x: -68.84, y: -1747.88, z: 27.97}},
    {type: DeliveryContractType.Products, reward: 300, target: {x: 158.92, y: -1465.83, z: 27.64}},
    {type: DeliveryContractType.ScrapMetal, reward: 300, target: {x: -456.83, y: -1715.34, z: 18.14}}
]

// Инициализация модуля
function initialize() {
    mp.events.add({
        [Events.TRUCKER_WORK]: startWork,
        [Events.TRUCKER_SALARY]: getSalary,
        [Events.TRUCKER_SELECT_CONTRACT]: onContractSelected
    })
    createVehicles()
    createNpcs()
    setInterval(() => {
        contracts.forEach(c => c.reward = Misc.randomInt(250, 350))
    }, 600000)
}

// Обработчик начала работы
function startWork(player) {
    if (!player.hasLicense(LicenseType.heavyVehicle)) {
        const message = `${locale('player', 'noLicense')} ${locale('radialMenu', 'category')} C`
        return Notify.showError(player, message)
    }
    if (!player.enoughLevel(MIN_LVL)) return
    if (player.startWorkAs(JobType.trucker)) {
        player.set(PlayerKey.IS_TRUCKER, true)
        applyWorkClothes(player)
        Notify.showInfo(player, locale('trucker', 'toStart'))
    }
}

// Обработчик получения зарплаты
function getSalary(player) {
    if (player.workAs(JobType.trucker)) {
        player.reset(
            PlayerKey.IS_TRUCKER,
            PlayerKey.DELIVERY_CONTRACT,
            PlayerKey.TRUCKER_ON_TARGET
        )
        player.call(Events.TRUCKER_HIDE_TARGET)
        player.finishWork()
    }
    player.applySalary(JobType.trucker)
}

// Обработчик выбора контракта
function onContractSelected(player, contract) {
    if (!player.workAs(JobType.trucker)) {
        Notify.showError(player, locale('trucker', 'needBeTrucker'))
        return
    }
    if (player.has(PlayerKey.DELIVERY_CONTRACT)) {
        Notify.showError(player, locale('trucker', 'alreadyHasContract'))
        return
    }
    player.set(PlayerKey.DELIVERY_CONTRACT, contract)
    Notify.showInfo(player, locale('trucker', 'takedContract'))
}

// Создает фуры и прицепы
function createVehicles() {
    data.vehicles.forEach(v => {
        setupSpawnInfo(v)
        const vehicle = VehiclesManager.createGeneral(v)
        vehicle.set(GENERAL_VEHICLE_KEY, VehicleTypes.truck)
    })
    data.trailers.forEach(t => {
        t.mainColor = 24
        VehiclesManager.createGeneral(t)
    })
}

// Настраивает данные для спавна фур
function setupSpawnInfo(info) {
    info.numberPlate = 'Truck'
    info.fuelTank = 400.0
    info.fuelRate = 12.5
    info.mainColor = 70
    info.secondColor = 1
}

// Создает нпс дальнобойщиков
function createNpcs() {
    data.npcs.forEach(e => {
        PointCreator.createBlip(e.position, 477, 21, PLACE_NAME)
        const name = locale('npc', e.name)
        const shape = PointCreator.createNpc(e.model, e.position, e.heading, name)
        shape.set(NAME_KEY, name)
        shape.set(CONTRACTS_KEY, e.contracts)
        shape.onEnter = comeToNpc
        shape.onExit = moveAwayFromNpc
    })
}

// Игрок подошел к нпс
function comeToNpc(player) {
    if (player.vehicle) return
    Notify.showHint(player, locale('hint', 'npcTalk'), this.get(NAME_KEY))
    player.set(PlayerKey.NPC_MENU, true)
    const contractsIndexes = this.get(CONTRACTS_KEY)
    const data = {
        contracts: contracts.filter((c, i) => contractsIndexes.includes(i)),
        selectCallback: Events.TRUCKER_SELECT_CONTRACT
    }
    player.call(Events.SETUP_MENU, [JSON.stringify({name: 'TruckerMenu', data})])
}

// Игрок отошел от нпс
function moveAwayFromNpc(player) {
    if (player.vehicle) return
    Notify.hideHint(player)
    player.reset(PlayerKey.NPC_MENU)
}

initialize()