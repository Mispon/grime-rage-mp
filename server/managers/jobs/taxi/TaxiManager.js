import peds from '../../../data/peds.json'
import data from './taxi-data.json'
import * as Events from '../../../../global/constants/ServerEvents'
import { IS_TAXI_DRIVER, VEHICLE_OVER_SPEED } from '../../../../global/constants/PlayerVariables'
import { ONE_KILOMETER } from '../../../../global/constants/VehicleVariables'
import VehicleTypes from '../../../enums/VehicleType'
import Misc from '../../../utils/Misc'
import Notify from '../../../utils/Notify'
import VectorHelper from '../../../utils/VectorHelper'
import { locale } from '../../../../global/Locale'

/*
** Логика работы в такси
*/

export const ORDER_TIMEOUT = 'TaxiOrderTimeout'
export const ORDER_COST = 'TaxiOrderCost'
const KM_PRICE = 25
const TAXI_SPEED = 35 // м/с

const rewardByLevel = {
    1: {salary: 2, exp: 24, jobExp: 12, tip: 12},
    2: {salary: 4, exp: 27, jobExp: 12, tip: 15},
    3: {salary: 6, exp: 30, jobExp: 10, tip: 17},
    4: {salary: 8, exp: 33, jobExp: 12, tip: 20},
    5: {salary: 10, exp: 36, jobExp: 0, tip: 25}
}

// Инициализация модуля
function initialize() {
    mp.events.add({
        'playerEnterVehicle': onEnterVehicle,
        [Events.TAXI_COMPLETE_ORDER]: completeOrder,
        [Events.TAXI_OVER_SPEED]: taxiOverSpeed,
        [Events.TAXI_TIME_IS_OUT]: orderTimeIsOver,
    })
}

// Обработчик входа в транспорт
function onEnterVehicle(player, vehicle) {
    if (!(vehicle.isSuch(VehicleTypes.taxi) && vehicle.canOpen(player, IS_TAXI_DRIVER))) return
    if (!player.has(ORDER_COST)) {
        createOrder(player)
    }    
}

// Создает новый заказ такси
function createOrder(player) {
    player.call(Events.CALL_UI, [Events.TAXI_WAIT_ORDER])
    const data = getOrderData()
    player.set(ORDER_COST, data.orderCost)
    if (data.client.isHurry)
        player.set(ORDER_TIMEOUT, false)
    else
        player.set(VEHICLE_OVER_SPEED, 0)
    player.call(Events.TAXI_START_ORDER, [JSON.stringify(data)])
}

// Обработчик завершения заказа
function completeOrder(player) {
    const cost = player.get(ORDER_COST)
    const reward = rewardByLevel[player.props.job.level]
    let salary = cost + (hasTip(player) ? reward.tip : 0)
    player.setSalary(salary)
    player.setJobExp(reward.jobExp)
    player.setExp(reward.exp)
    player.reset(VEHICLE_OVER_SPEED, ORDER_TIMEOUT)
    createOrder(player)
}

// Проверяет, положены ли чаевые водителю
function hasTip(player) {
    return player.has(ORDER_TIMEOUT) 
        ? player.get(ORDER_TIMEOUT) != true
        : player.get(VEHICLE_OVER_SPEED) < 5
}

// Обработчик превышения скорости
function taxiOverSpeed(player) {
    let count = player.get(VEHICLE_OVER_SPEED)
    Notify.showWarn(player, `${locale('drivingSchool', 'overSpeed')} ${++count} / 5`)
    player.set(VEHICLE_OVER_SPEED, count)
}

// Обработчик истечения времени чаевых
function orderTimeIsOver(player) {
    player.set(ORDER_TIMEOUT, true)
}

// Возвращает объект данных заказа
function getOrderData() {
    const beginPoint = data.points.begin[Misc.randomInt(0, data.points.begin.length)]
    const endPoint = chooseTargetPoint(beginPoint.position)
    const distance = VectorHelper.distance(beginPoint.position, endPoint)
    const time = calcOrderTime(distance)
    const cost = Math.floor(distance / ONE_KILOMETER * KM_PRICE)
    return {
        begin: beginPoint.position,
        end: endPoint,
        orderTime: time,
        orderCost: cost,
        client: {
            model: peds[Misc.randomInt(0, peds.length)],
            position: beginPoint.clientPosition,
            heading: beginPoint.clientHeading,
            isHurry: Misc.randomBool()
        }
    }
}

// Определяет отдаленную целевую позицию
function chooseTargetPoint(beginPosition) {
    let attempts = 0
    let result
    do {
        result = data.points.end[Misc.randomInt(0, data.points.end.length)]
        attempts++
    } while(VectorHelper.distance(beginPosition, result) < 1000 && attempts < 10)
    return result
}

// Рассчитывает время доставки клиента для чаевых
function calcOrderTime(distance) {    
    const kilometers = distance / ONE_KILOMETER
    return Math.floor(kilometers * 1000 / TAXI_SPEED)
}

initialize()