import data from './taxi-data.json'
import * as Events from '../../../../global/constants/ServerEvents'
import { NPC_MENU, IS_TAXI_DRIVER, VEHICLE_OVER_SPEED } from '../../../../global/constants/PlayerVariables'
import { GENERAL_VEHICLE_KEY } from '../../../../global/constants/VehicleVariables'
import { ORDER_TIMEOUT } from './TaxiManager'
import LicenseType from '../../../../global/enums/LicenseType'
import VehicleTypes from '../../../enums/VehicleType'
import * as MainPosition from '../../../constants/MainPositions'
import VehiclesManager from '../../vehicles/VehiclesManager'
import JobType from '../../../enums/JobType'
import PointCreator from '../../../utils/PointCreator'
import Notify from '../../../utils/Notify'
import { applyWorkClothes } from '../JobClothesController'
import { locale } from '../../../../global/Locale'

/*
** Таксопарк
*/

const MIN_LVL = 5
const PLACE_NAME = locale('place', 'taxi')
const NPC_NAME = locale('npc', 'taxi')

// Инициализация таксопарка
function initialize() {
    mp.events.add({
        [Events.TAXI_WORK]: startWork,
        [Events.TAXI_SALARY]: getSalary
    })
    createVehicles()
    createNpc()
}

// Обработчик начала работы в такси
function startWork(player) {
    if (!player.hasLicense(LicenseType.lightVehicle)) {
        const message = `${locale('player', 'noLicense')} ${locale('radialMenu', 'category')} B`
        return Notify.showError(player, message)
    }
    if (!player.enoughLevel(MIN_LVL)) return
    if (player.startWorkAs(JobType.taxi)) {        
        player.set(IS_TAXI_DRIVER, true)
        applyWorkClothes(player)
        Notify.showInfo(player, locale('taxiDriver', 'toStart'))
    }
}

// Обработчик получения зарплаты
function getSalary(player) {
    if (player.workAs(JobType.taxi)) {
        player.reset(IS_TAXI_DRIVER, VEHICLE_OVER_SPEED, ORDER_TIMEOUT)        
        player.call(Events.TAXI_REMOVE_ORDER)
        player.call(Events.CALL_UI, [Events.RESET_TIMER])
        player.finishWork()
    }    
    player.applySalary(JobType.taxi)
}

// Создает машины такси
function createVehicles() {
    data.vehicles.forEach(v => {
        setupSpawnInfo(v)
        const vehicle = VehiclesManager.createGeneral(v)
        vehicle.set(GENERAL_VEHICLE_KEY, VehicleTypes.taxi)
    })
}

// Настраивает данные для спавна автобусов
function setupSpawnInfo(info) {
    info.model = 'taxi'
    info.numberPlate = 'Taxi'
    info.fuelTank = 70.0
    info.fuelRate = 1.5
    info.mainColor = 42
    info.secondColor = 42
}

// Создает нпс
function createNpc() {
    PointCreator.createBlip(MainPosition.TaxiStation, 56, 46, PLACE_NAME)
    let shape = PointCreator.createNpc(data.npc.model, MainPosition.TaxiStation, data.npc.heading, NPC_NAME)
    shape.onEnter = comeToNpc
    shape.onExit = moveAwayFromNpc
}

// Игрок подошел к нпс
function comeToNpc(player) {
    if (player.vehicle) return
    Notify.showHint(player, locale('hint', 'npcTalk'), NPC_NAME)
    player.set(NPC_MENU, true)
    const data = {startWork: Events.TAXI_WORK, getSalary: Events.TAXI_SALARY}
    player.call(Events.SETUP_MENU, [JSON.stringify({name: 'JobMenu', data})])
}

// Игрок отошел от нпс
function moveAwayFromNpc(player) {
    if (player.vehicle) return
    Notify.hideHint(player)
    player.reset(NPC_MENU)
}

initialize()