import data from './bus-depot-data.json'
import * as Events from '../../../../global/constants/ServerEvents'
import { NPC_MENU, IS_BUS_DRIVER } from '../../../../global/constants/PlayerVariables'
import { GENERAL_VEHICLE_KEY } from '../../../../global/constants/VehicleVariables'
import { POINT_TYPE, POINT_INDEX } from './BusDriverManager'
import * as MainPosition from '../../../constants/MainPositions'
import JobType from '../../../enums/JobType'
import BusRoute from '../../../enums/BusRoute'
import VehicleTypes from '../../../enums/VehicleType'
import LicenseType from '../../../../global/enums/LicenseType'
import VehiclesManager from '../../vehicles/VehiclesManager'
import Notify from '../../../utils/Notify'
import PointCreator from '../../../utils/PointCreator'
import { applyWorkClothes } from '../JobClothesController'
import { locale } from '../../../../global/Locale'

/*
** Логика автобусного депо
*/

const MIN_LVL = 3
const NPC_NAME = locale('npc', 'busDepot')
const PLACE_NAME = locale('place', 'busDepot')

// Количество водителей на каждом из маршрутов в данный момент
const driversOnRoute = {
    [BusRoute.red]: 0,
    [BusRoute.green]: 0,
    [BusRoute.blue]: 0,
    [BusRoute.yellow]: 0
}

// Инициализация модуля
function initialize() {
    mp.events.add({
        [Events.BD_WORK]: startWork,
        [Events.BD_SALARY]: getSalary
    })
    createNpc()
    createVehicles()
}

// Обработчик начала работы водителем автобуса
function startWork(player) {
    if (!player.hasLicense(LicenseType.heavyVehicle)) {
        const message = `${locale('player', 'noLicense')} ${locale('radialMenu', 'category')} C`
        return Notify.showError(player, message)
    }
    if (!player.enoughLevel(MIN_LVL)) return
    if (player.startWorkAs(JobType.busDriver)) {
        const route = getFreestRoute() 
        player.set(IS_BUS_DRIVER, true)       
        player.set(POINT_TYPE, route)
        player.set(POINT_INDEX, -1)
        applyWorkClothes(player)
        Notify.showInfo(player, locale('busDriver', 'toStart'))
    }
}

// Обработчик получения зарплаты
function getSalary(player) {
    if (player.workAs(JobType.busDriver)) {
        finishWork(player)
    }    
    player.applySalary(JobType.busDriver)
}

// Завершение работы
function finishWork(player) {
    const route = player.get(POINT_TYPE)
    driversOnRoute[route] -= 1
    player.reset(IS_BUS_DRIVER, POINT_TYPE, POINT_INDEX)
    player.finishWork()
}

// Возвращает самый свободный маршрут
function getFreestRoute() {
    let key = ''
    let count = Number.MAX_SAFE_INTEGER
    for (let route in driversOnRoute) {
        const value = driversOnRoute[route]
        if (count <= value) continue
        count = value
        key = route
    }
    driversOnRoute[key] += 1
    return key
}

// Создает транспорт
function createVehicles() {
    data.vehicles.forEach(v => {
        setupSpawnInfo(v)
        let vehicle = VehiclesManager.createGeneral(v)
        vehicle.set(GENERAL_VEHICLE_KEY, VehicleTypes.bus)
    })
}

// Настраивает данные для спавна автобусов
function setupSpawnInfo(info) {
    info.numberPlate = 'Bus'
    info.heading = -20.0
    info.fuelTank = 250.0
    info.fuelRate = 10.0
    info.mainColor = 0
    info.secondColor = 0
}

// Создает нпс
function createNpc() {
    PointCreator.createBlip(MainPosition.BusDepot, 513, 47, PLACE_NAME)
    let shape = PointCreator.createNpc(data.npc.model, MainPosition.BusDepot, data.npc.heading, NPC_NAME)
    shape.onEnter = comeToNpc
    shape.onExit = moveAwayFromNpc
}

// Игрок подошел к нпс
function comeToNpc(player) {
    if (player.vehicle) return
    Notify.showHint(player, locale('hint', 'npcTalk'), NPC_NAME)
    player.set(NPC_MENU, true)
    const data = {startWork: Events.BD_WORK, getSalary: Events.BD_SALARY}
    player.call(Events.SETUP_MENU, [JSON.stringify({name: 'JobMenu', data})])
}

// Игрок отошел от нпс
function moveAwayFromNpc(player) {
    if (player.vehicle) return
    Notify.hideHint(player)
    player.reset(NPC_MENU)
}

initialize()