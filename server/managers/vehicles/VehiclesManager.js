import { TOGGLE_ENGINE, TOGGLE_LOCK, SET_FUEL } from '../../../global/constants/ServerEvents'
import VehicleBehavior from './VehicleBehavior'
import Notify from '../../utils/Notify'
import { locale } from '../../../global/Locale'

/*
** Управление транспортом сервера
*/
class VehiclesManager {
    constructor() {
        if (!VehiclesManager.instance) {            
            mp.events.add({
                [TOGGLE_ENGINE]: this.toggleEngine,
                [TOGGLE_LOCK]: this.toggleLock,
                [SET_FUEL]: this.setFuel
            })
            setInterval(() => {
                restoreGeneral()
                syncPersonal()
            }, 300000)
            VehiclesManager.instance = this
        }
        return VehiclesManager.instance
    }

    // Создает личный транспорт
    create(position, heading, dimension, data) {
        data.position = position
        data.heading = heading
        data.dimension = dimension
        return new VehicleBehavior(data)
    }

    // Создает общий транспорт
    createGeneral(generalData) {
        let data = convertData(generalData)
        let vehicle = new VehicleBehavior(data)
        vehicle.props.isGeneral = true
        return vehicle
    }

    // Переключает состояние мотора
    toggleEngine(player) {
        let vehicle = player.vehicle
        if (!vehicle) return
        vehicle.engine = !vehicle.engine
    }

    // Переключает состояние сигнализации
    toggleLock(player) {
        const vehicle = instance.getNearest(player.position)
        if (!vehicle) {
            Notify.showError(player, locale('vehicle', 'noNearest'))
            return
        }
        if (!vehicle.isOwner(player)) return
        vehicle.locked = !vehicle.locked
    }

    // Записывает кол-во бензина
    setFuel(player, fuel) {
        if (!player.vehicle) return
        player.vehicle.props.fuel = fuel
        if (fuel <= 0.01) {
            player.vehicle.engine = false
            Notify.showWarn(player, locale('vehicle', 'noFuel'))
        }
    }

    // Возвращает ближайший транспорт к указанной позиции
    getNearest(position) {
        let result = null
        let resultDist = Number.MAX_SAFE_INTEGER
        const vehicles = mp.vehicles.toArray().filter(v => v.dist(position) <= 10.0)
        vehicles.forEach(v => {
            const dist = v.dist(position)
            if (dist < resultDist) {
                result = v
                resultDist = dist
            }
        })
        return result
    }
}

// Восстанавливает общий транспорт
const restoreGeneral = () => {
    const vehicles = mp.vehicles.toArray().filter(v => v.props.isGeneral)
    vehicles.forEach(vehicle => {
        // todo: если уничтожено, то восстановить сразу
        if (isAfk(vehicle)) {
            if (vehicle.isAdmin || vehicle.isRented)
                vehicle.destroy()
            else
                vehicle.restore()
        }
    })  
}

// Синхронизирует личный транспорт
const syncPersonal = () => {
    /*const vehicles = mp.vehicles.toArray().filter(v => !v.props.isGeneral)
    vehicles.forEach(v => {
        console.log(v.props.model)
    })*/
}

// Проверяет, давно ли брошен транспорт
const isAfk = vehicle => {
    const afk = vehicle.props.afk 
    return afk && Date.now() - afk > 600000
}

// Конвертирует данные общественного транспорта в общую модель
const convertData = generalData => {
    let data = generalData
    data.fuel = generalData.fuelTank
    data.tuning = {
        mainColor: generalData.mainColor,
        secondColor: generalData.secondColor
    }
    return data
}

const instance = new VehiclesManager()

export default instance