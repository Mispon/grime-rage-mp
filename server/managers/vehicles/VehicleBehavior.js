import { GENERAL_VEHICLE_KEY, DRIVER_KEY } from '../../../global/constants/VehicleVariables'
import VectorHelper from '../../utils/VectorHelper'
import DataBehavior from '../../utils/DataBehavior'
import Notify from '../../utils/Notify'
import { locale } from '../../../global/Locale'

/*
** Обертка транспорта сервера
*/
class VehicleBehavior {
    constructor(data) {
        try {
            const vehicle = mp.vehicles.new(mp.joaat(data.model), VectorHelper.convert(data.position), {
                numberPlate: data.numberPlate,
                heading: data.heading,
                dimension: data.dimension,
                locked: data.locked,
                engine: false
            })
            new DataBehavior(vehicle)
            this.setup(vehicle, data)
            return vehicle
        } catch(e) {
            console.log(e.message)
        }        
    }

    // Сетапит транспорт
    setup(vehicle, data) {
        setTuning(vehicle, data.tuning)
        vehicle.props = data
        vehicle.props.fuel = data.fuelTank
        vehicle.isOwner = this.isOwner
        vehicle.setDriver = this.setDriver
        vehicle.getDriver = this.getDriver
        vehicle.fill = this.fill
        vehicle.canOpen = this.canOpen
        vehicle.isSuch = this.isSuch
        vehicle.restore = this.restore
    }

    // Проверяет, является ли игрок владельцем
    isOwner(player) {
        if (this.props.ownerId !== player.props.id) {
            Notify.showError(player, locale('player', 'noOwner'))
            return false
        }
        return true
    }

    // Устанавливает водителя
    setDriver(player) {
        this.set(DRIVER_KEY, player)
    }

    // Возвращает водителя
    getDriver() {
        return this.get(DRIVER_KEY)
    }

    // Заправляет транспорт
    fill(liters) {
        this.props.fuel += liters
        if (this.props.fuel > this.props.fuelTank) {
            this.props.fuel = this.props.fuelTank
        }
    }

    // Может ли данный игрок открыть машину
    canOpen(player, key) {
        if (!player.has(key)) {
            Notify.showError(player, locale('player', 'unavailableVehicle'))
            player.removeFromVehicle()
            this.setDriver(null)
            return false
        }
        return true
    }

    // Является ли общий транспорт требуемого типа
    isSuch(type) {
        if (!this.props.isGeneral) return false
        return this.get(GENERAL_VEHICLE_KEY) === type
    }

    // Восстанавливает исходное состояние транспорта
    restore() {
        this.engine = false
        this.rotation = new mp.Vector3(0, 0, this.props.heading)
        this.position = this.props.position
        this.props.fuel = this.props.fuelTank
        this.props.afk = null
        this.repair()
    }
}

// Применяет настройки тюнинга
const setTuning = (vehicle, tuning) => {
    vehicle.setColor(tuning.mainColor, tuning.secondColor)
}

module.exports = VehicleBehavior