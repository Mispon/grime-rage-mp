import { UPDATE_HUD } from '../../../../global/constants/ServerEvents'
import PlayerJobBehavior from './PlayerJobBehavior'
import PlayerLevelBehavior from './PlayerLevelBehavior'
import PlayerClothesBehavior from './PlayerClothesBehavior'
import DataBehavior from '../../../utils/DataBehavior'
import Notify from '../../../utils/Notify'
import DateHelper from '../../../utils/DateHelper'
import { locale } from '../../../../global/Locale'

/*
** Добавляет дополнительное поведение игрока
*/
class PlayerBehavior {
    constructor(player) {
        this.setupBehaviour(player)
        new PlayerJobBehavior(player)
        new PlayerLevelBehavior(player)
        new PlayerClothesBehavior(player)
        new DataBehavior(player)
        player.name = player.props.name
        player.health = player.props.health
        if (player.props.position) {
            player.position = player.props.position
        }
        updateAccountData(player)
        player.updateHUD()
    }

    // Расширяет функциональность игрока
    setupBehaviour(player) {
        player.setMoney = this.setMoney
        player.setBank = this.setBank
        player.enoughMoney = this.enoughMoney
        player.hasLicense = this.hasLicense
        player.isPremium = this.isPremium
        player.updateHUD = this.updateHUD
        player.isMale = this.isMale
    }

    // Добавляет указанную сумму к наличному балансу
    setMoney(amount) {
        if (amount === 0) return
        this.props.money += amount
        this.updateHUD()
    }

    // Добавляет указанную сумму к банку игрока
    setBank(amount) {
        if (amount === 0) return
        this.props.bank += amount
        this.updateHUD()
    }

    // Проверяет, достаточно ли у игрока денег
    enoughMoney(amount) {
        if (this.props.money < amount) {
            Notify.showError(this, locale('player', 'noMoney'))
            return false
        }
        return true
    }

    // Обновляет данные игрока в интерфейсе 
    updateHUD() {
        const data = {
            money: this.props.money,
            bank: this.props.bank,
            level: this.props.level,
            exp: this.getExpPercentage(),
            satiety: parseInt(this.props.satiety),
            thirst: parseInt(this.props.thirst)
        }
        this.call(UPDATE_HUD, [JSON.stringify(data)])
    }

    // Проверяет наличие лицензии
    hasLicense(type) {
        return this.props.driver.licenses.includes(type)
    }

    // Проверяет, активен ли премиум
    isPremium() {
        const premium = this.props.account.premium
        if (!premium) return false
        return !DateHelper.timeLeft(premium)
    }

    // Проверяет, является ли персонаж мужского пола
    isMale() {
        return this.props.gender === 0
    }
}

// Обновляет различные данные игрока
const updateAccountData = player => {
    player.props.account.lastLogin = new Date()
    // todo: player.props.account.daysInRow =
}

module.exports = PlayerBehavior