/*
** Перечисление маршрутов автобусов
*/
const BusRoute = Object.freeze({
    red: 'RedRoute',
    green: 'GreenRoute',
    blue: 'BlueRoute',
    yellow: 'YellowRoute'
})

export default BusRoute