/*
** Обработчики колшейпов
*/
mp.events.add("playerEnterColshape", (player, shape) => {
    if (shape.onEnter)
        shape.onEnter(player)
})

mp.events.add("playerExitColshape", (player, shape) => {
    if (shape.onExit)
        shape.onExit(player)
})