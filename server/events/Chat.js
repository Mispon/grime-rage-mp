/*
** Обработчик эвентов чата
*/

mp.events.add("playerChat", processChatMessage)

// Обработчик сообщения в чат
function processChatMessage(player, message) {
    // todo: накрутить команды
    mp.players.broadcastInRange(player.position, 15, `!{#4e9ded}${player.name}: !{#ffffff}${message}`)
}