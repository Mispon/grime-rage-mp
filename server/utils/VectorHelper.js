/*
** Вспомогательные методы для работы с векторами
*/
class VectorHelper {
    constructor() {
        if (!VectorHelper.instance) {
            VectorHelper.instance = this
        }
        return VectorHelper.instance
    }

    // Конвертирует объект в вектор
    convert(value) {
        return new mp.Vector3(value.x, value.y, value.z)
    }

    // Добавляет значение по высоте
    addZ(vector, value) {
        return new mp.Vector3(vector.x, vector.y, vector.z + value)
    }

    // Возвращает расстояние между векторами
    distance(start, end) {
        const x = end.x - start.x
        const y = end.y - start.y
        const z = end.z - start.z
        return Math.sqrt((x * x) + (y * y) + (z * z)).toFixed(2)
    }
}

const instance = new VectorHelper()

export default instance