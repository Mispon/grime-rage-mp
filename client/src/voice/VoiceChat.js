import { ADD_LISTENER, REMOVE_LISTENER, HUD_SET_MICRO } from '../../../global/constants/ServerEvents'
import { N } from '../constants/Keys'
import Browser from '../browser/Browser'
import { chatActive } from '../'

const USE_3D = true
const USE_AUTO_VOLUME = false
const MAX_RANGE = 50.0

mp.voiceChat.muted = true

mp.events.add({
    "playerQuit": player => {
        if (player.isListening) {
            voiceManager.remove(player, false)
        }
    },
    "render": () => {
        if (chatActive) return
        if (mp.keys.isDown(N) && mp.voiceChat.muted) {            
            mp.voiceChat.muted = false
            Browser.trigger(HUD_SET_MICRO, true)
        }
        else if (mp.keys.isUp(N) && !mp.voiceChat.muted) {            
            mp.voiceChat.muted = true
            Browser.trigger(HUD_SET_MICRO, false)
        }
    }
})

// Управление голосовым чатом
let voiceManager = {
    listeners: [],

    add(player) {
        this.listeners.push(player)
        player.isListening = true
        mp.events.callRemote(ADD_LISTENER, player)

        if (USE_AUTO_VOLUME)
            player.voiceAutoVolume = true
        else
            player.voiceVolume = 1.0

        if (USE_3D) player.voice3d = true
    },

    remove(player, notify) {
        let index = this.listeners.indexOf(player)
        if (index !== -1) this.listeners.splice(index, 1)
        player.isListening = false
        if (notify) mp.events.callRemote(REMOVE_LISTENER, player)
    },

    update(position) {
        this.listeners.forEach(other => {
            if (other.handle !== 0) {
                let otherPosition = other.position
                let distance = getDistance(position, otherPosition)
                if (distance > MAX_RANGE)
                    this.remove(other, true)
                else if (!USE_AUTO_VOLUME)
                    other.voiceVolume = 1 - (distance / MAX_RANGE)
            } 
            else
                this.remove(other, true)
        })
    }
}

// Обновление слушателей
function updateVoice() {
    let player = mp.players.local
    let position = player.position

    mp.players.forEachInStreamRange(other => {
        if (player != other && !other.isListening) {
            let otherPosition = other.position
            let distance = getDistance(position, otherPosition)
            if (distance <= MAX_RANGE) voiceManager.add(other)
        }        
    })

    voiceManager.update(position)
}

// Возвращает раcстояние между игроками
function getDistance(position, otherPosition) {
    return mp.game.system.vdist(position.x, position.y, position.z, otherPosition.x, otherPosition.y, otherPosition.z)
}

setInterval(updateVoice, 500)