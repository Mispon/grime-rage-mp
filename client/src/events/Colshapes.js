/*
** Клиентские обработчики колшейпов
*/
mp.events.add("playerEnterColshape", shape => {
    if (shape.onEnter)
        shape.onEnter()
})

mp.events.add("playerExitColshape", shape => {
    if (shape.onExit)
        shape.onExit()
})