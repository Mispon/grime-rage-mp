import Browser from './browser/Browser'
import * as Events from '../../global/constants/ServerEvents'
import {F3, T, ENTER} from './constants/Keys'

mp.gui.chat.colors = true
const player = mp.players.local

export let chatActive

// Обработчик готовности UI
mp.events.add({
    'guiReady': () => {
        Browser.load()
        Browser.setCursor(false)
        mp.events.callRemote(Events.GUI_READY)
    },
    'uiException': msg => mp.gui.chat.push(msg),
    [Events.CALL_REMOTE]: (eventName, ...args) => mp.events.callRemote(eventName, ...args),
    [Events.CALL_UI]: (eventName, ...args) => Browser.trigger(eventName, ...args),
    [Events.UPDATE_HUD]: args => Browser.triggerJson(Events.UPDATE_HUD, args),
    [Events.CREATE_NPCS]: createPeds
})

// Cоздает нпс
function createPeds(data) {
    const npcs = JSON.parse(data)
    npcs.forEach(npc => {
        const model = mp.game.joaat(npc.model)
        mp.peds.new(model, npc.position, npc.heading, sp => sp.setAlpha(250), npc.dimension)
    })      
}

// Основной клиентский апдейт
setInterval(() => {
    updateStreetName()
}, 1000)

// Обновляет название текущей улицы
function updateStreetName() {
    const position = player.position
    const street = mp.game.pathfind.getStreetNameAtCoord(position.x, position.y, position.z, 0, 0)
    const streetName = mp.game.ui.getStreetNameFromHashKey(street.streetName)
    Browser.trigger(Events.HUD_SET_STREET, streetName)
}

// Временная хрень, пока курсор обрабатывается не стабильно
mp.keys.bind(F3, true, () => mp.gui.cursor.show(false, false))

// Обработчики чата
mp.keys.bind(T, true, () => {
    if (!chatActive) chatActive = true
})
mp.keys.bind(ENTER, true, () => {
    if (chatActive) chatActive = false
})