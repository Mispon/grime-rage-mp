import * as Events from '../../../global/constants/ServerEvents'
import Browser from '../browser/Browser'

const MIN_FUEL = 0.01

const player = mp.players.local
let vehicle = null
let sendIntervalId

class VehicleManager {
    constructor() {
        if (!VehicleManager.instance) {                      
            this.fuel = null
            this.fuelTank = null
            this.fuelRate = 0
            this.speed = 0
            this.setupEvents()
            VehicleManager.instance = this  
        }
        return VehicleManager.instance
    }

    // Добавляет обработчики на серверные эвенты
    setupEvents() {
        mp.events.add({
            [Events.HUD_TOGGLE_VEHICLE]: this.toggleHUD.bind(this),
            'render': () => {
                if (!vehicle) return
                this.calcFuel()
            }
        })
    }

    // Переключает HUD транспорта
    toggleHUD(isEnter, fuel = null, fuelTank = null, fuelRate = 0) {
        Browser.trigger(Events.HUD_TOGGLE_VEHICLE, isEnter)
        if (isEnter) { 
            vehicle = player.vehicle           
            this.fuel = fuel
            this.fuelTank = fuelTank
            this.fuelRate = fuelRate
            this.sendStats()
            sendIntervalId = setInterval(() => {
                this.sendStats()
                if (this.fuel)
                    mp.events.callRemote(Events.SET_FUEL, this.fuel)
            }, 1000)
        } else {
            vehicle = null
            clearInterval(sendIntervalId)
        }
    }

    // Рассчитывает расход бензина
    calcFuel() {    
        if (!(this.fuel && vehicle.getIsEngineRunning())) return      
        if (this.fuel > MIN_FUEL) 
            this.fuel -= this.getFuelDecrease()
    }

    // Отправляет данные в интерфейс
    sendStats() {
        this.speed = (vehicle.getSpeed() * 3.6).toFixed(2)  
        const data = {speed: parseInt(this.speed), fuel: null}
        if (this.fuel) {
            data.fuel = parseInt(this.fuel),
            data.fuelTank = this.fuelTank
        }
        Browser.triggerJson(Events.HUD_UPDATE_VEHICLE, JSON.stringify(data))
    }

    // Возвращает расход топлива
    getFuelDecrease() {
        const rpm = (vehicle.rpm * 5000).toFixed(0)
        let gear = vehicle.gear
        if (gear === 0) gear = 1
        return (rpm + this.speed) / gear * this.fuelRate * Math.pow(5, -13)
    }
}

const instance = new VehicleManager()