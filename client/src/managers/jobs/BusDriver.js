import { BD_NEXT_POINT, BD_HIDE_ROUTE } from '../../../../global/constants/ServerEvents'
import { LANG } from '../../../../global/constants/PlayerVariables'
import Misc from '../../utils/Misc'
import { locale } from '../../../../global/Locale'

let marker
let blip

const player = mp.players.local

mp.events.add({
    [BD_NEXT_POINT]: showNextPoint,
    [BD_HIDE_ROUTE]: hideRoute
})

// Показывает следующую точку маршрута
function showNextPoint(position) {
    mp.game.ui.setNewWaypoint(position.x, position.y)
    if (marker === undefined || blip === undefined) {
        marker = mp.markers.new(1, position, 3, {color: [255, 186, 84, 255]})
        blip = mp.blips.new(1, position, {color: 3, name: locale('busDriver', 'busStop', player.getVariable(LANG))})
    }
    else {
        marker.position = position
        blip.setCoords(position)
    }
}

// Скрывает маршрут
function hideRoute() {
    Misc.removeEntity(marker)
    Misc.removeEntity(blip)
    mp.game.ui.setNewWaypoint(player.position.x, player.position.y)
}