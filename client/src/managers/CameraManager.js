/*
** Логика взаимодействия с камерами
*/
class CameraManager {
    constructor() {
        if (!CameraManager.instance) {
            CameraManager.instance = this
        }
        return CameraManager.instance
    }

    // Создает новую камеру
    createNew(position, rotation, lookAt, fov, name = 'customCamera') {
        const camera = mp.cameras.new(name, position, rotation, fov)
        if (lookAt) camera.pointAtCoord(lookAt)
        camera.setActive(true)
        return camera
    }
}

const instance = new CameraManager()

export default instance