import {AUTH_SHOW, AUTH_HIDE} from '../../../../global/constants/ServerEvents'
import Browser from '../../browser/Browser'
import CameraManager from '../../managers/CameraManager'
import { toggleEnvironment } from './CharacterCreator'

/*
** Клиентская логика окна авторизации
*/

const cameraPosition = new mp.Vector3(1569.68, -367.94, 226.57)
const cameraRotation = new mp.Vector3(0, 0, 132.63)

mp.events.add({
    [AUTH_SHOW]: showAuth,
    [AUTH_HIDE]: hideAuth
})

// Показывает окно авторизации
function showAuth() {
    CameraManager.createNew(cameraPosition, cameraRotation, null, 40)
    toggleEnvironment(false)
    Browser.trigger(AUTH_SHOW)
    Browser.onWindowsToggle(true)
}

// Скрывает окно авторизации
function hideAuth() {
    toggleEnvironment(true)
    Browser.trigger(AUTH_HIDE)
    Browser.onWindowsToggle(false)
}