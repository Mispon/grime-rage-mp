import * as Events from '../../../../global/constants/ServerEvents'
import Browser from '../../browser/Browser'
import Misc from '../../utils/Misc'

let marker
let speedCheckerId

const player = mp.players.local

mp.events.add({
    [Events.START_THEORY_EXAM]: startTheory,
    [Events.THEORY_EXAM_FINISH]: finishTheory,
    [Events.START_PRACTICE_EXAM]: startPractice,
    [Events.NEXT_EXAM_POINT]: showNextPoint,
    [Events.HIDE_EXAM_POINT]: resetExam,
})

// начало теоретического экзамена
function startTheory() {
    Browser.trigger(Events.START_THEORY_EXAM)
    Browser.onWindowsToggle(true)
}

// завершение теоретического экзамена
function finishTheory(result) {
    mp.events.callRemote(Events.THEORY_EXAM_FINISH, result)
    Browser.onWindowsToggle(false)
}

// Обработчик начала практического экзамена
function startPractice() {
    speedCheckerId = setInterval(checkSpeed, 5000)
}

// Проверяет скорость
function checkSpeed() {
    if (!player.vehicle) return
    let speed = player.vehicle.getSpeed() * 3.6
    if (speed > 60) {
        mp.events.callRemote(Events.DS_OVER_SPEED)
    }
}

// Показывает следующую точку маршрута экзамена
function showNextPoint(position) {
    mp.game.ui.setNewWaypoint(position.x, position.y)
    if (marker === undefined)
        marker = mp.markers.new(1, position, 3, {color: [218, 0, 234, 150]})
    else
        marker.position = position
}

// Сбрасывает состояние текущего экзамена
function resetExam() {
    Misc.removeEntity(marker)
    mp.game.ui.setNewWaypoint(player.position.x, player.position.y)
    clearInterval(speedCheckerId)
}
