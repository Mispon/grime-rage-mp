/*
** Всевозможные вспомогательные методы
*/
class Misc {
    constructor() {
        if (!Misc.instance) {
            Misc.instance = this
        }
        return Misc.instance
    }

    // Удаляет клиентский объект
    removeEntity(entity) {
        try {
            if (entity) entity.destroy()                
        }
        catch(e) {
            mp.gui.chat.push(`Misc.remoteEntity: ${e.message}`)
        }
    }
}

const instance = new Misc()

export default instance