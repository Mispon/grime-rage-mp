import { SHOW_DIALOG, DIALOG_SELECT } from '../../../../global/constants/ServerEvents'
import Browser from '../Browser'

let isOpen = false

mp.events.add({
    [SHOW_DIALOG]: showDialogue,
    [DIALOG_SELECT]: onSelect
})

// показывает диалоговое окно
function showDialogue(...args) {
    isOpen = true
    Browser.trigger(SHOW_DIALOG, ...args)
    Browser.onWindowsToggle(isOpen)
}

// обработчик выбора
function onSelect(callback) {
    isOpen = false
    Browser.onWindowsToggle(isOpen)
    if (callback) mp.events.callRemote(callback)
}