import { SETUP_MENU, TOGGLE_MENU } from '../../../../global/constants/ServerEvents'
import { NPC_MENU } from '../../../../global/constants/PlayerVariables'
import { E } from '../../constants/Keys'
import Browser from '../Browser'
import { lookingAt } from '../../utils/LookingHelper'
import { chatActive } from '../../'

const player = mp.players.local
let entity = null
let isOpen = false

mp.events.add({
    'render': onRender,
    [SETUP_MENU]: setupMenu
})

// Вызывается в каждом кадре
function onRender() {
    try {
        entity = lookingAt()
        if (entity == null) return
        mp.game.graphics.drawText("E", [entity.position.x, entity.position.y, entity.position.z + 1], {
            font: 2,
            color: [0, 255, 0, 255],
            scale: [0.5, 0.5],
            outline: true
        })
    } catch(e) {
        mp.gui.chat.push(e.message)
    }    
}

// интервал прослушивания нажатий на кнопку активации меню
setInterval(() => {
    if (chatActive) return
    if (mp.keys.isDown(E) && !isOpen) {
        let type = getMenuType()
        if (type != '') toggleMenu(type)      
    }
    else if (mp.keys.isUp(E) && isOpen) {
        toggleMenu()
    }
}, 200)

// Возвращает тип меню
function getMenuType() {
    if (player.vehicle) return 'vehicle'
    if (entity != null) return entity.type
    return (player.getVariable(NPC_MENU)) ? 'npc' : ''
}

// изменяет видимость меню
function toggleMenu(menuType = null) {
    isOpen = menuType != null
    Browser.trigger(TOGGLE_MENU, menuType)
    Browser.onWindowsToggle(isOpen)
}

// устанавливает данные для меню
function setupMenu(data) {    
    Browser.triggerJson(SETUP_MENU, data)
}