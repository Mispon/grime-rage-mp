/*
** Взаимодействие с CEF
*/
class Browser {
    constructor() {
        if (!Browser.instance) {
            Browser.instance = this
            this.openedWindows = 0
        }
        return Browser.instance
    }

    // загружает браузер
    load() {
        this.browser = mp.browsers.new('package://ui/index.html')
    }

    // отправляет событие в UI
    trigger(eventName, ...args) {
        try {
            this.browser.execute(`trigger('${eventName}', '${JSON.stringify([...args])}')`)
        } catch (e) {
            mp.gui.chat.push(`trigger: error on "${eventName}" call`)
        }
    }

    // отправляет событие в UI
    triggerJson(eventName, jsonData) {
        this.browser.execute(`trigger('${eventName}', '${jsonData}')`)
    }

    // устанавливает активность курсора
    setCursor(value) {
        mp.gui.cursor.show(value, value)
    }

    // отслеживает изменения видимости окон
    onWindowsToggle(state) {
        this.openedWindows += state ? 1 : -1
        if (this.openedWindows < 0) this.openedWindows = 0
        this.setCursor(this.openedWindows > 0)
    }
}

const instance = new Browser()

export default instance