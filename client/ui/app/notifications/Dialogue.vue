<template>
    <div :class="{dialog: true, active: isOpen, inactive: !isOpen}">
        <section class="dialog-question">
            <h1>{{ title }}</h1>
        </section>
        <section class="dialog-info">
            <p>{{ description }}</p>
        </section>
        <section class="dialog-buttons">
            <button class="dialog-accept" v-on:click="click(true)">{{ getButtonName('accept') }}</button>
            <button class="dialog-decline" v-on:click="click(false)">{{ getButtonName('decline') }}</button>
        </section>
    </div>
</template>

<script>
import '../../styles/notifications/dialogue.scss'

import { SHOW_DIALOG, DIALOG_SELECT } from '../../../../global/constants/ServerEvents'
import { locale } from '../../../../global/Locale'

export default {
    props: ['lang'],
    data() {
        return {
            isOpen: false,
            title: '',
            description: '',
            onAccept: '',
            onDecline: ''
        }
    },
    methods: {
        show(args) {
            this.title = args[0]
            this.description = args[1]
            this.onAccept = args[2]
            this.onDecline = args[3]
            this.isOpen = true
        },

        click(isAccepted) {
            if (!this.isOpen) return
            const callback = isAccepted ? this.onAccept : this.onDecline
            mp.trigger(DIALOG_SELECT, callback)
            this.isOpen = false
        },

        getButtonName(value) {
            return locale('button', value, this.lang)
        }
    },
    created() {
        EventManager.on(SHOW_DIALOG, this.show)
    }
}
</script>
