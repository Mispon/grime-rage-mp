/*
** Типы контрактов доставки (для дальнобойщиков и пилотов)
*/
const DeliveryContractType = Object.freeze({
    Corn: 0,
    Crude: 1,
    Tools: 2,
    WorkshopWood: 3,
    BuildingsWood: 4,
    Dyes: 5,
    Fuel: 6,
    Products: 7,
    ScrapMetal: 8,
    Crop: 9,
    MechanicalDrawings: 10,
    Fertilizer: 11,
    MilitaryEquipment: 12,
    CollectedPrototypes: 13,
    MilitaryReports: 14
})

export default DeliveryContractType