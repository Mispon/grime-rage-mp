/*
** Пол персонажа
*/
const GenderType = Object.freeze({
    Male: 0,
    Female: 1
})

export default GenderType