/*
** Валидация пользовательского ввода
*/

// Проверяет, являются ли строки пустыми
export const isEmpty = (...values) => {
    let result = false
    values.forEach(v => {
        if (!v || v.length === 0) {
            result = true
        }
    })
    return result
}

// Проверяет, является ли строка почтой
export const isEmail = email => {
    const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    return re.test(email)
}

// Проверяет, содержит ли строка только латинские символы
export const isOnlyLatin = str => {
    const re = /^[a-zA-Z]*$/
    return re.test(str)
}