/*
** Константы игрока
*/

// Общие
export const LANG = 'Lang'
export const NPC_MENU = 'HasNpcMenu'
export const LAST_POSITION = 'Player::LastPosition'
export const LAST_DIMENSION = 'Player::LastDimension'

// Работы
export const IS_COURIER = 'Player::Courier'
export const IS_BUS_DRIVER = 'Player::BusDriver'
export const IS_TAXI_DRIVER = 'Player::TaxiDriver'
export const IS_TRUCKER = 'Player::Trucker'
export const IS_PILOT = 'Player::Pilot'

// Транспорт
export const VEHICLE_DAMAGES = 'VehicleDamagesCount'
export const VEHICLE_OVER_SPEED = 'VehicleOverSpeed'

// Всякое
export const COURIER_ON_TARGET = 'Courier::OnDeliveryTarget'
export const TRUCKER_ON_TARGET = 'Trucker::OnTarget'
export const PILOT_ON_TARGET = 'Pilot::OnTarget'
export const DELIVERY_CONTRACT = 'DeliveryContract'
export const DRIVING_EXAM = 'DrivingPracticeExam'